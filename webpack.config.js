import path from 'path'

export default {
    mode: 'development',
    entry: {
        mapa: './src/js/mapa.js',
        addimg: './src/js/addimg.js',
        viewmap: './src/js/viewmap.js',
        maphome: './src/js/maphome.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve('public/js')
    }
}