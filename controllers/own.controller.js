import { unlink } from 'node:fs/promises'
import { validationResult } from 'express-validator'
import { Price, Category, Own } from '../models/index.model.js'


const admin = async (req, res) => {

    // Leer QueryString
    const { pagina: paginaNow } = req.query

    const expersion = /^\d*$/
    const firstCaracter = paginaNow.charAt(0)

    if(!expersion.test(paginaNow) || paginaNow == 0 || firstCaracter == 0) {
        return res.redirect('/mis-propiedades?pagina=1')
    }

    try {
        const { id } = req.user

        // Límite y Offset para el paginador
        const limit = 2
        const offset = ((paginaNow * limit) - limit)

        const [owns, total] = await Promise.all([
            Own.findAll({
                limit,
                offset,
                where: {
                    userId: id
                },
                include: [
                    { model: Category, as: 'category' },
                    { model: Price, as: 'price'}
                ]
            }),
            Own.count({
                where: {
                    userId: id
                }
            }),  
        ])

        if(paginaNow > Math.ceil(total / limit)) {
            return res.redirect(`/mis-propiedades?pagina=${Math.ceil(total / limit)}`)
        }

        res.render('own/admin', {
            page: 'Mis Propiedades',
            owns,
            pages: Math.ceil(total / limit),
            paginaNow: Number(paginaNow),
            total,
            offset,
            limit
        })

    } catch (error) {
        console.log(error);
    }

}

// Formulario para crear una nueva propiedad

const create = async (req, res) => {

    // Consulatar Modelo de Precio y Categorias
    const [categories, prices] = await Promise.all([
        Category.findAll(),
        Price.findAll()
    ])

    res.render('own/create', {
        page: 'Crear Propiedad',
        categories, // Object literal de JS categories: categories
        prices,
        data: {}
    })
}

const saveCreate = async (req, res) => {

    // Validación
    let result = validationResult(req)
    
    if(!result.isEmpty()) {

        // Consulatar Modelo de Precio y Categorias
        const [categories, prices] = await Promise.all([
        Category.findAll(),
        Price.findAll()
        ])

        return res.render('own/create', {
            page: 'Crear Propiedad',
            categories,
            prices,
            errors: result.array(),
            data: req.body
        })
    }
    
    // Crear un Registro

    const { title, description, bedrooms, rooms, bathrooms, calle, lat, lng, price: priceId, category: categoryId } = req.body // Al usar destructuring, se puede cambiar en nombre a la variable como en price y category
    const { id: userId } = req.user

    try {
        const ownesSaving = await Own.create({
            title,
            description,
            bedrooms,
            rooms,
            bathrooms,
            calle,
            lat,
            lng,
            priceId, // otra opción es no cambiar el nombre en el destructuring y asignarlo aquí, sería: price: priceId
            categoryId,
            userId,
            imagen: ''
        })

        const { id } = ownesSaving

        res.redirect(`/propiedades/agregar-imagen/${id}`)


    } catch (error) {
        console.log(error);
    }
}

const addImg = async (req, res) => {

    const { id } = req.params

    // Validar que la propiedad exita
    const owns = await Own.findByPk(id)

    if(!owns) {
        return res.redirect('/mis-propiedades')
    }

    // Validar que la propiedad no esté publicada

    if(owns.publicado) {
        return res.redirect('/mis-propiedades')
    }

    // Validar que la propiedad pertenece a quien visita esta página

    if(req.user.id.toString() !== owns.userId.toString()) {
        return res.redirect('/mis-propiedades')
    }


    res.render('own/add-img', {
        page: `Agregar Imagenes: ${owns.title}`,
        owns
    })
}

const storeImg = async (req, res, next) => {

    const { id } = req.params

    // Validar que la propiedad exita
    const owns = await Own.findByPk(id)

    if(!owns) {
        return res.redirect('/mis-propiedades')
    }

    // Validar que la propiedad no esté publicada

    if(owns.publicado) {
        return res.redirect('/mis-propiedades')
    }

    // Validar que la propiedad pertenece a quien visita esta página

    if(req.user.id.toString() !== owns.userId.toString()) {
        return res.redirect('/mis-propiedades')
    }

    try {
        // Almacenar la imagen y publicar propieda
        owns.imagen = req.file.filename
        owns.publicado = 1

        await owns.save()

        next()

    } catch (error) {
        console.log(error);
    }
}

const getEditOwn = async (req, res) => {

    const { id } = req.params

    // Validar que la propiedad exita
    const own = await Own.findByPk(id)

    if(!own) {
        return res.redirect('/mis-propiedades')
    }

    // Revisar que quien visita la URL, es quien creo la propiedad

    if(own.userId.toString() !== req.user.id.toString()) {
        return res.redirect('/mis-propiedades')
    }

    // Consulatar Modelo de Precio y Categorias
    const [categories, prices] = await Promise.all([
        Category.findAll(),
        Price.findAll()
    ])

    res.render('own/edit', {
        page: `Editar Propiedad: ${own.title}`,
        categories, // Object literal de JS categories: categories
        prices,
        data: own
    })

}

const postSaveEdit = async (req, res) => {
    // Validación
    let result = validationResult(req)
        
    if(!result.isEmpty()) {

        // Consulatar Modelo de Precio y Categorias
        const [categories, prices] = await Promise.all([
            Category.findAll(),
            Price.findAll()
        ])
  
        return res.render('own/edit', {
            page: `Editar Propiedad: ${req.body.title}`,
            categories,
            prices,
            errors: result.array(),
            data: req.body,
        })
    }

    const { id } = req.params

    // Validar que la propiedad exita
    const own = await Own.findByPk(id)

    if(!own) {
        return res.redirect('/mis-propiedades')
    }

    // Revisar que quien visita la URL, es quien creo la propiedad

    if(own.userId.toString() !== req.user.id.toString()) {
        return res.redirect('/mis-propiedades')
    }


    // Reescribir el objeto y actualizarlo

    try {
        const { title, description, bedrooms, rooms, bathrooms, calle, lat, lng, price: priceId, category: categoryId } = req.body

        own.set({
            title,
            description,
            bedrooms,
            rooms,
            bathrooms,
            calle,
            lat,
            lng,
            priceId,
            categoryId
        })

        await own.save()

        res.redirect('/mis-propiedades')

    } catch (error) {
        console.log(error);
    }
}


const postDelete = async (req, res) => {

    const { id } = req.params

    // Validar que la propiedad exita
    const own = await Own.findByPk(id)

    if(!own) {
        return res.redirect('/mis-propiedades')
    }

    // Revisar que quien visita la URL, es quien creo la propiedad

    if(own.userId.toString() !== req.user.id.toString()) {
        return res.redirect('/mis-propiedades')
    }

    // Eliminar imagne
    await unlink(`public/uploads/${own.imagen}`)
    console.log(`Se eliminó la imagen ${own.imagen}`)

    // Eliminar la propiedad
    await own.destroy()
    res.redirect('/mis-propiedades')

}

const getViewOwn = async (req, res) => {

    const { id } = req.params

    // Comprobar que la propiedad exita
    const own = await Own.findByPk(id, {
        include : [
            {model: Price, as: 'price'},
            {model: Category, as: 'category'},
        ]
    })

    if (!own) {
        return res.redirect('/404')
    }

    res.render('own/show', {
        own,
        pag: own.title
    })
}
export {
    admin,
    create,
    saveCreate,
    addImg,
    storeImg,
    getEditOwn,
    postSaveEdit,
    postDelete,
    getViewOwn
}