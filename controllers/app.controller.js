import { Own, Price, Category, } from '../models/index.model.js'


const getHome = async (req, res) => {

    const [ categories, prices, casas, departamentos ] = await Promise.all([
        Category.findAll({raw: true}),
        Price.findAll({raw: true}),
        Own.findAll({
            limit: 3,
            where: {
                categoryId: 1
            },
            includes: [
                {
                    model: Price,
                    as: 'precio'
                }
            ],
            order: [
                ['createdAt', 'DESC']
            ]
        }),
        Own.findAll({
            limit: 3,
            where: {
                categoryId: 2
            },
            includes: [
                {
                    model: Price,
                    as: 'precio'
                }
            ],
            order: [
                ['createdAt', 'DESC']
            ]
        }),
    ])

    res.render('home', {
        page: 'Inicio',
        categories,
        prices,
        casas,
        departamentos,
    })

}


const getCategory = (req, res) => {
    
}

const getNotPage = (req, res) => {
    
}

const getSerch = (req, res) => {
    
}

export {
    getHome,
    getCategory,
    getNotPage,
    getSerch,
}