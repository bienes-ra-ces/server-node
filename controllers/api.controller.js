import { Own, Price, Category, } from '../models/index.model.js'

const propiedades = async (req, res) => {

    const propiedades = await Own.findAll({
        include: [
            {model: Price, as: 'price'},
            {model: Category, as: 'category'}
        ]
    })

    res.json(
        propiedades
    )
}

export {
    propiedades
}