import bcrypt from 'bcrypt'

const users = [
    {
        name: 'Fernando',
        email: 'fer@fer.es',
        confirmed: 1,
        password: bcrypt.hashSync('123456', 10)
    },
    {
        name: 'Fernando',
        email: 'fer@fer.com',
        confirmed: 1,
        password: bcrypt.hashSync('123456', 10)
    }
]

export default users