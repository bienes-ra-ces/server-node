(function() {
    const lat = 41.3909483;
    const lng = 2.1808026;
    const mapa = L.map('map-home').setView([lat, lng ], 10);


    let markers = new L.FeatureGroup().addTo(mapa)

    let propiedades = []
   
    // Filtros
    const filtros = {
        categoria: '',
        precio: ''
    }

    const categoriasSelect = document.querySelector('#categorias')
    const preciosSelect = document.querySelector('#precios')


    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mapa);

    // Filtros de Categorias y Precios
    categoriasSelect.addEventListener('change', eve => {
        filtros.categoria = +eve.target.value
        filtrarPropiedades()
    })

    preciosSelect.addEventListener('change', eve => {
        filtros.precio = +eve.target.value
        filtrarPropiedades()
    })


    const obtenerPropiedades = async () => {
        try {
            const url ='/api/propiedades'
            const respuesta = await fetch(url)
            propiedades = await respuesta.json()

            mostrarPropiedades(propiedades)
            
        } catch (error) {
            console.log(error);
        }
    }
    const mostrarPropiedades = propiedades => {

        // Limpiar los markers previos
        markers.clearLayers()

        propiedades.forEach(propiedad => {
            // Agregar los pines
            const marker = new L.marker([propiedad?.lat, propiedad?.lng], {
                autoPan: true
            })
            .addTo(mapa)
            .bindPopup(`
                <h1 class="text-xl font-extrabold uppercase my-2">${propiedad?.title}</h1>
                <img src="/uploads/${propiedad?.imagen}" alt="Imagen de la propiedad ${propiedad?.title}">
                <p class="text-gray-600 font-bold">${propiedad?.price.nombre}</p>
                <a href="/propiedad/${propiedad?.id}"   class="block p-2 text-center font-bold uppercase text-indigo-600">Ver Propiedad</a>
            `)

            markers.addLayer(marker)
        });
    }

    const filtrarPropiedades = () => {   
        const resultado = propiedades.filter(filtarCategorias).filter(filtarPrecios)
        mostrarPropiedades(resultado)
    }
    const filtarCategorias = propiedad => filtros.categoria ? propiedad.categoryId === filtros.categoria : propiedad
    const filtarPrecios = propiedad => filtros.precio ? propiedad.priceId === filtros.precio : propiedad

    obtenerPropiedades()
})()