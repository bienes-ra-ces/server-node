import express from "express";
import { getHome, getCategory, getNotPage, getSerch } from "../controllers/app.controller.js";

const router = express.Router()


// Página de Inicio
router.get('/', getHome)

// Categorías
router.get('/categoria/:id', getCategory)

// 404
router.get('/404', getNotPage)

// Buscador
router.get('/buscador', getSerch)

export default router