/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/maphome.js":
/*!***************************!*\
  !*** ./src/js/maphome.js ***!
  \***************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n(function() {\n    const lat = 41.3909483;\n    const lng = 2.1808026;\n    const mapa = L.map('map-home').setView([lat, lng ], 10);\n\n\n    let markers = new L.FeatureGroup().addTo(mapa)\n\n    let propiedades = []\n   \n    // Filtros\n    const filtros = {\n        categoria: '',\n        precio: ''\n    }\n\n    const categoriasSelect = document.querySelector('#categorias')\n    const preciosSelect = document.querySelector('#precios')\n\n\n    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {\n        attribution: '&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors'\n    }).addTo(mapa);\n\n    // Filtros de Categorias y Precios\n    categoriasSelect.addEventListener('change', eve => {\n        filtros.categoria = +eve.target.value\n        filtrarPropiedades()\n    })\n\n    preciosSelect.addEventListener('change', eve => {\n        filtros.precio = +eve.target.value\n        filtrarPropiedades()\n    })\n\n\n    const obtenerPropiedades = async () => {\n        try {\n            const url ='/api/propiedades'\n            const respuesta = await fetch(url)\n            propiedades = await respuesta.json()\n\n            mostrarPropiedades(propiedades)\n            \n        } catch (error) {\n            console.log(error);\n        }\n    }\n    const mostrarPropiedades = propiedades => {\n\n        // Limpiar los markers previos\n        markers.clearLayers()\n\n        propiedades.forEach(propiedad => {\n            // Agregar los pines\n            const marker = new L.marker([propiedad?.lat, propiedad?.lng], {\n                autoPan: true\n            })\n            .addTo(mapa)\n            .bindPopup(`\n                <h1 class=\"text-xl font-extrabold uppercase my-2\">${propiedad?.title}</h1>\n                <img src=\"/uploads/${propiedad?.imagen}\" alt=\"Imagen de la propiedad ${propiedad?.title}\">\n                <p class=\"text-gray-600 font-bold\">${propiedad?.price.nombre}</p>\n                <a href=\"/propiedad/${propiedad?.id}\"   class=\"block p-2 text-center font-bold uppercase text-indigo-600\">Ver Propiedad</a>\n            `)\n\n            markers.addLayer(marker)\n        });\n    }\n\n    const filtrarPropiedades = () => {   \n        const resultado = propiedades.filter(filtarCategorias).filter(filtarPrecios)\n        mostrarPropiedades(resultado)\n    }\n    const filtarCategorias = propiedad => filtros.categoria ? propiedad.categoryId === filtros.categoria : propiedad\n    const filtarPrecios = propiedad => filtros.precio ? propiedad.priceId === filtros.precio : propiedad\n\n    obtenerPropiedades()\n})()\n\n//# sourceURL=webpack://bienesraices/./src/js/maphome.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/js/maphome.js"](0, __webpack_exports__, __webpack_require__);
/******/ 	
/******/ })()
;